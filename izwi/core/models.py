# Copyright 2024-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from typing import Literal

from pydantic import BaseModel
from pydantic import Field
from typing_extensions import TypedDict


class TranscoderMarkdownTranslation(TypedDict):
  title_level_1: str
  title_level_2: str
  title_level_x: str
  code_block: str
  image: str


class TranscoderModel(BaseModel):
  lang: str
  special_chars: dict[str, str]
  mdtr: TranscoderMarkdownTranslation


class SynthesizePayload(BaseModel):
  """
  Payload for text-to-speech synthesis requests.
  """
  voice: str
  input: str


class File(BaseModel):
  """
  Represents a file with its metadata.
  """
  size_bytes: int
  md5_digest: str


class Language(BaseModel):
  """
  Represents language metadata.
  """
  code: str
  family: str
  region: str
  name_native: str
  name_english: str
  country_english: str


class VoiceInfo(BaseModel):
  """
  Represents metadata about a voice.
  """
  name: str
  language: Language
  quality: Literal['x_low', 'low', 'medium', 'high']
  num_speakers: int
  speaker_id_map: dict[str, int]
  aliases: list[str]


class RawVoiceInfo(VoiceInfo):
  """
  Represents extended metadata about a voice, including file information and internal flags.
  """
  key: str
  files: dict[str, File]
  is_alias: bool = Field(default=False, exclude=True)
  loaded: bool = Field(default=False, exclude=True)

  def info(self) -> VoiceInfo:
    """
    Returns a simplified `VoiceInfo` object without internal fields.

    Returns:
      VoiceInfo: A `VoiceInfo` object containing only the public attributes.
    """
    return VoiceInfo(
      name=self.key,
      language=self.language,
      quality=self.quality,
      num_speakers=self.num_speakers,
      speaker_id_map=self.speaker_id_map,
      aliases=self.aliases,
    )
