# Copyright 2024-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from fastapi import Body
from fastapi import Depends
from fastapi import FastAPI
from fastapi import HTTPException
from fastapi import Query

from ..core.controller import Controller
from ..core.docstring import extract_docstring
from ..core.models import SynthesizePayload

TTS_ENDPOINTS = ['/synthesize', '/v1/audio/speech', '/audio/speech']


def create_tts_endpoints(tag: str, api: FastAPI, controller: Controller):
  # Dependency for synthesis parameters
  def get_synthesis_params(
    speaker_id: int = Query(
      default=controller.default_speaker_id,
      alias='speaker-id',
      description=(
        'The ID of the speaker to use for synthesis.\n\n'
        'Each voice may support multiple speakers, and the valid speaker IDs '
        'can be retrieved from the `/voices` or `/voices/{name}` endpoints. '
        'If not provided, the default speaker ID for the selected voice will be used.'
      ),
    ),
    length_scale: float = Query(
      default=controller.default_length_scale,
      alias='length-scale',
      description=(
        'The scale factor for phoneme length, which controls the speed of the speech.\n\n'
        'A value greater than `1.0` slows down the speech, while a value less than `1.0` speeds it up. '
        'For example, a value of `1.5` will make the speech 50% slower, while `0.8` will make it 20% faster. '
        'The default value is `1.0`, which represents the natural speed of the voice.'
      ),
    ),
    noise_scale: float = Query(
      default=controller.default_noise_scale,
      alias='noise-scale',
      description=(
        'The scale factor for generator noise, which controls the randomness and expressiveness of the speech.\n\n'
        'Higher values (e.g., `0.8`) make the speech more expressive but potentially less stable, '
        'while lower values (e.g., `0.2`) make the speech more monotonic and stable. '
        'The default value is typically around `0.667`, which provides a balance between expressiveness and stability.'
      ),
    ),
    noise_w: float = Query(
      default=controller.default_noise_w,
      alias='noise-w',
      description=(
        'The scale factor for phoneme width noise, which controls the variation in phoneme duration.\n\n'
        'Higher values (e.g., `1.0`) introduce more variation in the length of phonemes, making the speech sound more dynamic. '
        'Lower values (e.g., `0.5`) make the phoneme durations more consistent. '
        'The default value is typically around `0.8`, which provides a natural-sounding variation.'
      ),
    ),
    sentence_silence: float = Query(
      default=controller.default_sentence_silence,
      alias='sentence-silence',
      description=(
        'The duration of silence (in seconds) added after each sentence.\n\n'
        'This parameter is useful for adding natural pauses between sentences in longer texts. '
        'For example, a value of `0.5` adds a half-second pause after each sentence. '
        'The default value is typically around `0.2`, which provides a short, natural pause.'
      ),
    ),
  ):
    return {
      'speaker_id': speaker_id,
      'length_scale': length_scale,
      'noise_scale': noise_scale,
      'noise_w': noise_w,
      'sentence_silence': sentence_silence,
    }

  # Dependency for query-based payload
  def get_query_payload(
    voice: str = Query(
      ...,
      description=(
        'The name of the voice to use for synthesis.\n\n'
        'This parameter specifies the voice that will be used to convert the text into speech. '
        'Valid voices can be retrieved from the `/voices` endpoint. '
        'Ensure the voice name matches exactly with the available voices from the `/voices` endpoint.'
      ),
      example='fr_FR-siwis-medium',
    ),
    text: str = Query(
      ...,
      alias='input',
      description=(
        'The text to synthesize into speech.\n\n'
        'This parameter contains the text that will be converted into audio. '
        'It can be any string, but longer texts may take more time to process. '
        'Special characters and punctuation are supported, but ensure the text is properly encoded.'
      ),
      example='Bonjour, le monde ! Ceci est un exemple de synthèse vocale.',
    ),
  ) -> SynthesizePayload:
    return SynthesizePayload(voice=voice, input=text)

  # Dependency for body-based payload
  def get_body_payload(
    payload: SynthesizePayload = Body(
      ...,
      description=(
        'A JSON payload containing the voice and text for synthesis.\n\n'
        'This payload allows you to specify the voice and text to be synthesized. '
        'The `voice` field specifies the voice to use, and the `text` field contains the text to convert into speech. '
        'Valid voices can be retrieved from the `/voices` endpoint.'
      ),
      example=SynthesizePayload(
        voice='fr_FR-siwis-medium',
        input=(
          "Oremi n'est pas une technologie unique, mais un écosystème d'assistant personnel doté d'un orchestrateur central (Akọrin) "
          'qui intègre et coordonne divers modules (Injini, Izwi, Andika, Ohunerin) pour offrir une expérience utilisateur transparente.'
        ),
      ),
    ),
  ) -> SynthesizePayload:
    return payload

  async def synthesize_speech_from_payload(
    payload: SynthesizePayload = Depends(get_body_payload),
    params: dict = Depends(get_synthesis_params),
  ) -> bytes:
    """
    Synthesize speech from a JSON payload.

    This endpoint converts the provided text into speech using the specified
    voice and synthesis parameters. The `voice` and `text` must be provided
    in the request body as a JSON payload.
    """
    try:
      return controller.synthesize(
        payload,
        speaker_id=params['speaker_id'],
        length_scale=params['length_scale'],
        noise_scale=params['noise_scale'],
        noise_w=params['noise_w'],
        sentence_silence=params['sentence_silence'],
      )
    except ValueError as error:
      raise HTTPException(status_code=400, detail='Voice not found') from error

  for index, path in enumerate(TTS_ENDPOINTS):
    docstring = extract_docstring(synthesize_speech_from_payload)
    docstring += '\n\n**Aliases**\n' + '\n'.join([f'- `POST {item}`' for item in TTS_ENDPOINTS[1:]])

    decorate = api.post(
      path=path,
      tags=[tag],
      responses={
        200: {
          'content': {
            'audio/wav': {},
          },
        },
        400: {'description': 'Voice not found'},
      },
      include_in_schema=index == 0,
      description=docstring,
    )

    decorate(synthesize_speech_from_payload)

  @api.get(
    path='/synthesize',
    tags=[tag],
    responses={
      200: {
        'content': {
          'audio/wav': {},
        },
      },
      400: {'description': 'Voice or text not provided'},
    },
  )
  async def synthesize_speech_from_query(
    payload: SynthesizePayload = Depends(get_query_payload),
    params: dict = Depends(get_synthesis_params),
  ) -> bytes:
    """
    Synthesize speech from query parameters.

    This endpoint converts the provided text into speech using the specified
    voice and synthesis parameters. The `voice` and `text` must be provided
    as query parameters.
    """
    try:
      return controller.synthesize(
        payload,
        speaker_id=params['speaker_id'],
        length_scale=params['length_scale'],
        noise_scale=params['noise_scale'],
        noise_w=params['noise_w'],
        sentence_silence=params['sentence_silence'],
      )
    except ValueError as error:
      raise HTTPException(status_code=400, detail='Voice not found') from error

  @api.post(
    path='/synthesize/stream',
    tags=[tag],
    responses={
      200: {
        'content': {
          'audio/wav': {},
        },
      },
      400: {'description': 'Voice not found'},
    },
  )
  async def stream_speech_from_payload(
    payload: SynthesizePayload = Depends(get_body_payload),
    params: dict = Depends(get_synthesis_params),
  ) -> bytes:
    """
    Stream synthesized speech from a JSON payload.

    This endpoint converts the provided text into speech and streams the audio
    in real-time. The `voice` and `text` must be provided in the request body
    as a JSON payload.
    """
    try:
      return await controller.synthesize_stream(
        payload,
        speaker_id=params['speaker_id'],
        length_scale=params['length_scale'],
        noise_scale=params['noise_scale'],
        noise_w=params['noise_w'],
        sentence_silence=params['sentence_silence'],
      )
    except ValueError as error:
      raise HTTPException(status_code=400, detail='Voice not found') from error

  @api.get(
    path='/synthesize/stream',
    tags=[tag],
    responses={
      200: {
        'content': {
          'audio/wav': {},
        },
      },
      400: {'description': 'Voice or text not provided'},
    },
  )
  async def stream_speech_from_query(
    payload: SynthesizePayload = Depends(get_query_payload),
    params: dict = Depends(get_synthesis_params),
  ) -> bytes:
    """
    Stream synthesized speech from query parameters.

    This endpoint converts the provided text into speech and streams the audio
    in real-time. The `voice` and `text` must be provided as query parameters.
    """
    try:
      return await controller.synthesize_stream(
        payload,
        speaker_id=params['speaker_id'],
        length_scale=params['length_scale'],
        noise_scale=params['noise_scale'],
        noise_w=params['noise_w'],
        sentence_silence=params['sentence_silence'],
      )
    except ValueError as error:
      raise HTTPException(status_code=400, detail='Voice not found') from error
