# Oremi Izwi

[![Buy me a beer](https://img.shields.io/badge/Buy%20me-a%20beer-1f425f.svg)](https://www.buymeacoffee.com/demsking)

*Oremi Izwi* stands as the auditory cornerstone of the Oremi project, a
revolutionary initiative aimed at redefining the personal assistant landscape.

Leveraging cutting-edge TTS (Text-to-Speech) technology and built upon the
robust framework of [Piper](https://github.com/rhasspy/piper), this module
effortlessly transforms textual inputs into rich, lifelike speech, elevating
user interactions to a whole new level of realism and immersion. Notably, Oremi
Izwi now also supports streaming TTS endpoints, ensuring seamless and
uninterrupted delivery of audio content for an even smoother user experience.

Derived from the Zulu word for 'voice,' *Izwi* embodies the essence of vocal
communication, capturing the richness and diversity of human expression. By
infusing the Oremi personal assistant with this dynamic TTS capability,
*Oremi Izwi* not only facilitates seamless interaction but also fosters a deeper
connection between users and their digital companion, transforming mere commands
into engaging conversations and enhancing the overall user experience.

<p align="center">
  <img src="doc/logo.png" style="width: 250px; height: auto;" alt="Oremi Izwi logo">
</p>

## Documentation

Open the documentation URL to access the API documentation. In your web browser,
navigate to [https://demsking.gitlab.io/oremi-izwi](https://demsking.gitlab.io/oremi-izwi).

## Contribute

Please follow [CONTRIBUTING.md](https://gitlab.com/demsking/oremi-izwi/blob/main/CONTRIBUTING.md).

## Versioning

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards-compatible manner,
  and
- `PATCH` version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions
to the `MAJOR.MINOR.PATCH` format.

See [SemVer.org](https://semver.org/) for more details.

## License

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License.
You may obtain a copy of the License at [LICENSE](https://gitlab.com/demsking/oremi-izwi/blob/main/LICENSE).
