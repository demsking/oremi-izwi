# Copyright 2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
import asyncio
import io
import wave
from concurrent.futures import ThreadPoolExecutor

from fastapi import Response
from fastapi.responses import StreamingResponse
from piper import PiperVoice

from ..transcoder import Transcoder
from ..transcoder.en import English
from ..transcoder.fr import French
from .download import ensure_voice_exists
from .download import find_voice
from .download import get_voices_info
from .logger import logger
from .models import SynthesizePayload
from .models import VoiceInfo


class Controller:
  def __init__(
    self,
    *,
    use_cuda: bool = False,
    speaker_id: int | None = None,
    length_scale: float | None = None,
    noise_scale: float | None = None,
    noise_w: float | None = None,
    sentence_silence: float = 0.0,
  ):
    self.use_cuda = use_cuda
    self.default_speaker_id = speaker_id
    self.default_length_scale = length_scale
    self.default_noise_scale = noise_scale
    self.default_noise_w = noise_w
    self.default_sentence_silence = sentence_silence
    self.transcoder = Transcoder([English, French])
    self.voices_info, aliases_info = get_voices_info()
    self.voices_map: dict[str, VoiceInfo] = {
      name: voice.info() for name, voice in self.voices_info.items()
    }

    self.voices_listing = list(self.voices_map.values())
    self.voices_info.update(aliases_info)

  def load_voice(self, voice_name: str) -> tuple[VoiceInfo, PiperVoice]:
    # if voice_name not in voices_instances:
    voice_info = self.voices_info[voice_name]

    ensure_voice_exists(voice_info)
    model_path, config_path = find_voice(voice_name)

    voice = PiperVoice.load(
      model_path,
      config_path=config_path,
      use_cuda=self.use_cuda,
    )

    return self.voices_info[voice_name], voice

  def parse(self, *, voice: str, text: str) -> tuple[PiperVoice, str]:
    info, piper = self.load_voice(voice)
    lang = info.language.family
    text = self.transcoder.parse(lang, text=text)

    return piper, text

  def generate_audio_data(
    self,
    byte_stream: io.BytesIO,
    payload: SynthesizePayload,
    event: asyncio.Event,
    **synthesize_kwargs,
  ):
    """
    Generate audio data and write to a byte stream in WAV format.
    """
    logger.debug(f'Synthesizing parsed text: {payload.input}')
    piper, input = self.parse(voice=payload.voice, text=payload.input)
    logger.debug(f'Synthesizing parsed text: {input}')

    # pylint: disable=no-member
    with wave.open(byte_stream, 'wb') as wf:
      wf.setnchannels(1)  # Mono
      wf.setsampwidth(2)  # 16-bit
      wf.setframerate(piper.config.sample_rate)

      event.set()

      stream = piper.synthesize_stream_raw(input, **synthesize_kwargs)

      for audio_bytes in stream:
        wf.writeframes(audio_bytes)

    # Reset the pointer to the beginning of the stream
    byte_stream.seek(0)

  def audio_stream_generator(self, byte_stream: io.BytesIO):
    """
    Generator function to stream audio data.
    """
    while True:
      chunk = byte_stream.read(1024)
      if not chunk:
        break
      yield chunk

  async def synthesize_stream(self, payload: SynthesizePayload, **synthesize_kwargs):
    byte_stream = io.BytesIO()
    event = asyncio.Event()

    # Use ThreadPoolExecutor to generate audio data in a background thread
    with ThreadPoolExecutor() as executor:
      executor.submit(self.generate_audio_data, byte_stream, payload, event, **synthesize_kwargs)
      await event.wait()  # Ensure some data is written before starting to stream

    return StreamingResponse(
      content=self.audio_stream_generator(byte_stream),
      media_type='audio/wav',
      headers={
        'Accept-Ranges': 'bytes',  # Indicate byte-range support for streaming
      },
    )

  def synthesize(self, payload: SynthesizePayload, **synthesize_kwargs):
    logger.debug(f'Synthesizing text: {payload.input}')
    piper, input = self.parse(voice=payload.voice, text=payload.input)
    logger.debug(f'Synthesizing parsed text: {input}')

    with io.BytesIO() as wav_io:
      with wave.open(wav_io, 'wb') as wav_file:
        piper.synthesize(input, wav_file, **synthesize_kwargs)

      return Response(
        content=wav_io.getvalue(),
        media_type='audio/wav',
        headers={'Content-Disposition': 'attachment; filename=speech.wav'},
      )
