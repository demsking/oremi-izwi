# Copyright 2024-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
import multiprocessing

from oremi_core.vars import getenv

from .package import APP_NAME
from .package import APP_VERSION

USE_CUDA = getenv('RUNTIME', default='cpu').lower() == 'gpu'

MODELS_DIR = getenv('MODELS_DIR', default='/usr/local/cache/piper/models')
"""
Directory path for storing voice models. This is where downloaded models are saved and loaded
from during synthesis.
"""

PIPER_SPEAKER_ID = getenv('PIPER_SPEAKER_ID', default=0, expected_type=int)
"""
Speaker ID for the Piper application. This is used to select a specific speaker in multi-speaker
models.
"""

PIPER_LENGTH_SCALE = getenv('PIPER_LENGTH_SCALE', default=1.0, expected_type=float)
"""
Phoneme length scale for the Piper application. Controls the speed of speech. A value greater
than `1.0` slows down the speech, while a value less than `1.0` speeds it up.
"""

PIPER_NOISE_SCALE = getenv('PIPER_NOISE_SCALE', default=1.0, expected_type=float)
"""
Scale for generator noise in the Piper application. Controls the expressiveness of the speech.
Higher values make the speech more expressive but potentially less stable.
"""

PIPER_NOISE_W = getenv('PIPER_NOISE_W', default=1.0, expected_type=float)
"""
Width noise for phonemes in the Piper application. Controls the variation in phoneme duration.
Higher values introduce more variation, making the speech sound more dynamic.
"""

PIPER_SENTENCE_SILENCE = getenv('PIPER_SENTENCE_SILENCE', default=0.5, expected_type=float)
"""
Duration of silence (in seconds) after each sentence in the Piper application. Useful for adding
natural pauses between sentences.
"""

HTTP_AGENT = getenv('USER_AGENT', default=f'{APP_NAME}/{APP_VERSION}')
"""
User agent string for HTTP requests.

This string identifies the application and its version in HTTP requests, such as when downloading
voice models.
"""

CACHE_DIR = getenv('CACHE_DIR', default='/usr/local/cache')
"""
The directory in which to store cached data, such as downloaded models or temporary files.
"""

SERVER_HOST = getenv('SERVER_HOST')
"""
The host address on which the server will listen.
"""

SERVER_PORT = getenv('SERVER_PORT', default=5215, expected_type=int)
"""
The port on which the server will listen.
"""

ROOT_PATH = getenv('ROOT_PATH', default='/')
"""
The root path for the API, which defines the base URL path for all routes.
"""

LOG_FILE = getenv('LOG_FILE', default='')
"""
Path to the log file where application logs will be stored. If left empty, logs may be directed
to the console by default.
"""

LOG_LEVEL = getenv('LOG_LEVEL', default='INFO')
"""
The log level used to control the verbosity of application logs. Options include 'DEBUG', 'INFO',
'WARNING', 'ERROR', and 'CRITICAL'.
"""

WORKERS = getenv('WORKERS', default=multiprocessing.cpu_count(), expected_type=int)
"""
The number of worker processes to run for handling incoming API requests. By default, it uses
the number of CPU cores available on the system.
"""
