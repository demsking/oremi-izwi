# Copyright 2024-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
import os
from contextlib import asynccontextmanager

import httpx
from fastapi import FastAPI
from fastapi import Request
from fastapi import Response
from fastapi.responses import RedirectResponse

from .core import config
from .core.api import parse_path
from .core.controller import Controller
from .core.logger import logger
from .core.package import APP_VERSION
from .endpoints.docs import create_docs_endpoints
from .endpoints.tts import create_tts_endpoints
from .endpoints.version import create_version_endpoints
from .endpoints.voices import create_voices_endpoints


SERVICE_TITLE = 'Oremi Izwi Documentation'

_current_dir = os.path.dirname(os.path.abspath(__file__))
_readme_path = os.path.realpath(
  os.path.join(_current_dir, '..', 'DOCUMENTATION.md'),
)

with open(_readme_path, 'r', encoding='utf-8') as file:
  SERVICE_DESCRIPTION = file.read()

voices_tag = {
  'name': 'Voices Endpoints',
  'description': (
    'Endpoints related to retrieving and managing voice information for text-to-speech synthesis. '
    'These endpoints allow users to explore available voices, retrieve detailed information about specific voices, '
    'and preload voice models for faster synthesis. '
    'Each voice is characterized by attributes such as language, quality, number of speakers, and speaker IDs.'
  ),
}

tts_tag = {
  'name': 'TTS Endpoints',
  'description': (
    'Endpoints for text-to-speech (TTS) synthesis. These endpoints allow users to convert text into speech '
    'using specified voices and synthesis parameters. Both standard and streaming synthesis are supported, '
    'enabling real-time audio generation for applications requiring low-latency responses. '
    'Users can customize synthesis behavior using parameters such as speaker ID, phoneme length, noise scales, '
    'and sentence silence duration.'
  ),
}

version_tag = {
  'name': 'Server Endpoints',
  'description': (
    'Endpoints for retrieving server-related information, including the server\'s name and version. '
    'This allows clients to easily identify the server they are interacting with and its current version, '
    'ensuring transparency and compatibility in API usage.'
  ),
}

BASE_PATH = parse_path('/api')


@asynccontextmanager
async def lifespan(app: FastAPI):
  # execute the app lifespan atr the start of the context manager and at the end
  # logger.info(f'Allowed Origins: {ALLOWED_ORIGINS}')
  yield

api = FastAPI(
  docs_url=None,
  redoc_url=None,
  lifespan=lifespan,
  title=SERVICE_TITLE,
  version=APP_VERSION,
  description=SERVICE_DESCRIPTION,
  root_path=BASE_PATH,
  servers=[
    {'url': BASE_PATH},
  ],
  license_info={
    'name': 'Apache 2.0',
    'url': 'http://www.apache.org/licenses/LICENSE-2.0.html',
  },
  openapi_tags=[
    voices_tag,
    tts_tag,
    version_tag,
  ],
)

controller = Controller(
  use_cuda=config.USE_CUDA,
  speaker_id=config.PIPER_SPEAKER_ID,
  length_scale=config.PIPER_LENGTH_SCALE,
  noise_scale=config.PIPER_NOISE_SCALE,
  noise_w=config.PIPER_NOISE_W,
  sentence_silence=config.PIPER_SENTENCE_SILENCE,
)

create_voices_endpoints(voices_tag['name'], api, controller)
create_tts_endpoints(tts_tag['name'], api, controller)
create_version_endpoints(version_tag['name'], api)
create_docs_endpoints(api)


@api.get('/', response_class=RedirectResponse, include_in_schema=False)
async def index():
  return '/docs'


@api.exception_handler(httpx.HTTPStatusError)
async def httpx_http_status_error_handler(
  request: Request,
  error: httpx.HTTPStatusError,
) -> Response:
  logger.error(error)

  return Response(status_code=error.response.status_code)
