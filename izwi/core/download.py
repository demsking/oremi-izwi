# Copyright 2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Utility for downloading Piper voices.
# Original source: https://github.com/rhasspy/piper/blob/d09b7d02567958162ff86a4a42476740bc3b8a02/src/python_run/piper/download.py
import json
import os
from pathlib import Path

import httpx

from .config import HTTP_AGENT
from .file_hash import get_file_hash
from .logger import logger
from .models import RawVoiceInfo


URL_FORMAT = 'https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/{file}'

_VOICES_PATH = '/opt/rhasspy/piper/voices.json'
_MODELS_PATH = Path(os.getenv('PIPER_DOWNLOAD_DIR') or '/usr/local/cache/piper/models')

_SKIP_FILES = {'MODEL_CARD'}


def parse_voice_infos(data: dict[str, dict]) -> dict[str, RawVoiceInfo]:
  return {
    name: RawVoiceInfo.parse_obj(item) for name, item in data.items()
  }


def get_voices_info() -> tuple[dict[str, RawVoiceInfo], dict[str, RawVoiceInfo]]:
  """Loads available voices from downloaded or embedded JSON file."""
  logger.info(f'Loads available voices from {_VOICES_PATH}')

  voices_info: dict[str, dict] = {}
  aliases_info: dict[str, dict] = {}

  with open(_VOICES_PATH, 'r', encoding='utf-8') as voices_file:
    voices_info = json.load(voices_file)

  # Resolve aliases for backwards compatibility with old voice names
  for voice_info in voices_info.values():
    for voice_alias in voice_info.get('aliases', []):
      aliases_info[voice_alias] = {'is_alias': True, **voice_info}

  return parse_voice_infos(voices_info), parse_voice_infos(aliases_info)


def ensure_voice_exists(voice_info: RawVoiceInfo) -> None:
  if voice_info.loaded:
    return

  files_to_download: set[str] = set()

  # Check sizes/hashes
  for file_path, file_info in voice_info.files.items():
    if file_path in files_to_download:
      # Already planning to download
      continue

    file_name = Path(file_path).name
    if file_name in _SKIP_FILES:
      continue

    data_file_path = _MODELS_PATH / file_name
    logger.debug(f'Checking {data_file_path}')
    if not data_file_path.exists():
      logger.warning(f'Missing {data_file_path}')
      files_to_download.add(file_path)
      continue

    expected_size = file_info.size_bytes
    actual_size = data_file_path.stat().st_size
    if expected_size != actual_size:
      logger.warning(f'Wrong size (expected={expected_size}, actual={actual_size}) for {data_file_path}')
      files_to_download.add(file_path)
      continue

    expected_hash = file_info.md5_digest
    actual_hash = get_file_hash(data_file_path)
    if expected_hash != actual_hash:
      logger.warning(f'Wrong hash (expected={expected_hash}, actual={actual_hash}) for {data_file_path}')
      files_to_download.add(file_path)
      continue

  if (not voice_info.files) and (not files_to_download):
    raise ValueError(f'Unable to find or download voice: {voice_info.key}')

  # Download missing files
  for file_path in files_to_download:
    file_name = Path(file_path).name

    if file_name in _SKIP_FILES:
      continue

    file_url = URL_FORMAT.format(file=file_path)
    download_file_path = _MODELS_PATH / file_name
    download_file_path.parent.mkdir(parents=True, exist_ok=True)

    logger.info(f'Downloading {file_url} to {download_file_path}')
    download_file(file_url, download_file_path)
    logger.info(f'Downloaded {download_file_path} ({file_url})')

  voice_info.loaded = True


def download_file(file_url: str, download_file_path: str) -> None:
  response = httpx.get(
    url=file_url,
    timeout=5,
    follow_redirects=True,
    headers={'User-Agent': HTTP_AGENT},
  )

  response.raise_for_status()  # Raise exception for non-2xx status codes

  with open(download_file_path, 'wb') as downloaded_file:
    downloaded_file.write(response.content)


def find_voice(name: str) -> tuple[Path, Path]:
  onnx_path = _MODELS_PATH / f'{name}.onnx'
  config_path = _MODELS_PATH / f'{name}.onnx.json'

  if onnx_path.exists() and config_path.exists():
    return onnx_path, config_path

  raise ValueError(f'Missing files for voice {name}')
