**Oremi Izwi** stands as the auditory cornerstone of the Oremi project, a
revolutionary initiative aimed at redefining the personal assistant landscape.

Leveraging cutting-edge TTS (Text-to-Speech) technology and built upon the
robust framework of [Piper](https://github.com/rhasspy/piper), this module
effortlessly transforms textual inputs into rich, lifelike speech, elevating
user interactions to a whole new level of realism and immersion. Notably, Oremi
Izwi now also supports streaming TTS endpoints, ensuring seamless and
uninterrupted delivery of audio content for an even smoother user experience.

Derived from the Zulu word for 'voice,' *Izwi* embodies the essence of vocal
communication, capturing the richness and diversity of human expression. By
infusing the Oremi personal assistant with this dynamic TTS capability,
*Oremi Izwi* not only facilitates seamless interaction but also fosters a deeper
connection between users and their digital companion, transforming mere commands
into engaging conversations and enhancing the overall user experience.

## Features

Oremi Izwi offers a robust set of features designed to deliver high-quality text-to-speech synthesis with flexibility and ease of use. Below are the key features:

### Multiple Voices

- Choose from a wide variety of voices supporting different languages, accents, and styles.
- Voices are categorized by quality levels (`x_low`, `low`, `medium`, `high`) to suit various use cases, from lightweight applications to high-fidelity audio.

### Customizable Synthesis

- Fine-tune speech output with adjustable parameters:
  - **Speech Speed**: Control the speed of speech using the `length_scale` parameter.
  - **Expressiveness**: Adjust the `noise_scale` to make speech more expressive or monotonic.
  - **Phoneme Variation**: Use `noise_w` to control the variation in phoneme duration.
  - **Sentence Pauses**: Add natural pauses between sentences with the `sentence_silence` parameter.

### Real-Time Streaming

- Stream synthesized audio in real-time for low-latency applications.
- Ideal for interactive systems, live captioning, or any use case requiring immediate audio feedback.

### Multi-Speaker Support

- Some voices support multiple speakers within a single model, allowing you to switch between different speaker styles quickly.
- While multi-speaker models offer versatility, the quality of individual speakers may be slightly lower compared to single-speaker models.

### Voice Quality Levels

Voices are trained at one of four quality levels to balance performance and audio fidelity:

| Quality  | Audio Sample Rate | Model Size (Params) | Use Case                          |
|----------|-------------------|---------------------|-----------------------------------|
| `x_low`  | 16 kHz            | 5-7M               | Lightweight, low-resource systems |
| `low`    | 16 kHz            | 15-20M             | General-purpose applications     |
| `medium` | 22.05 kHz         | 15-20M             | High-quality for most use cases  |
| `high`   | 22.05 kHz         | 28-32M             | Premium audio for high fidelity  |

### Easy Integration

- **RESTful API**: Seamlessly integrate Oremi Izwi into your applications using a well-documented RESTful API.
- **Interactive Documentation**: Access the API documentation at `http://localhost:5215/docs` for detailed endpoint descriptions and examples.

### Docker Support

- Run the service locally or deploy it in production using Docker.
- Supports both CPU and GPU versions for optimized performance:
  - **CPU Version**: Tag format `<version>` (e.g., `v1.0.0`).
  - **GPU Version**: Tag format `<version>-gpu` (e.g., `v1.0.0-gpu`).

### Cross-Platform Compatibility

- The Docker image supports both `linux/amd64` and `linux/arm64` platforms,
  making it versatile for deployment on a wide range of systems.

### Scalability

- Designed to handle both small-scale and large-scale synthesis tasks, making
  it suitable for personal projects, enterprise applications, and everything
  in between.

## Docker Image

The [`demsking/oremi-izwi`](https://hub.docker.com/r/demsking/oremi-izwi) Docker
image is officially available on Docker Hub. The image is configured to expose
port `5215`, which serves the API and documentation endpoints.

### Platform Support

- **CPU Version**:
  - Supports both `linux/amd64` and `linux/arm64` platforms.
  - Ideal for running the service on CPU-only machines, including ARM-based
    systems like Raspberry Pi.

- **GPU Version**:
  - Supports only the `linux/amd64` platform.
  - Designed for systems with NVIDIA GPUs to leverage hardware acceleration for
    faster synthesis.

### Image Tags

The Docker image provides two types of tags to support different hardware
configurations:

1. **CPU Version**:
   - Tag format: `<version>`
   - Example: `latest`
   - Use this version if you are running the service on a CPU-only machine.

2. **GPU Version**:
   - Tag format: `<version>-gpu`
   - Example: `latest-gpu`
   - Use this version if you have a GPU available and want to leverage it for
     faster synthesis.

### Running the Container

**CPU Version**

To run the CPU version of the container, use the following command:

```bash
docker run -d -p 5215:5215 --name oremi-izwi --env-file <path_to_env_file> demsking/oremi-izwi:<version>
```

**GPU Version**

To run the GPU version of the container, ensure you have NVIDIA drivers and the NVIDIA Container Toolkit installed. Then, use the following command:

```bash
docker run -d -p 5215:5215 --gpus all --name oremi-izwi --env-file <path_to_env_file> demsking/oremi-izwi:<version>-gpu
```

### Accessing the API and Documentation

Once the container is running, you can access to the interactive API
documentation by visiting `http://localhost:5215/docs`.

## Environment Variables

Oremi Izwi can be configured using environment variables to customize its
behavior, performance, and logging. Below is a detailed breakdown of the
available variables:

### Model and Synthesis Configuration

These variables allow you to customize the model directory and synthesis
parameters.

- **`MODELS_DIR`**:
  Directory path for storing models.

  **Default**: `/usr/local/cache/piper/models`

- **`PIPER_SPEAKER_ID`**:
  Speaker ID for the Piper application. This is used to select a specific
  speaker in multi-speaker models.

  **Default**: `0`

- **`PIPER_LENGTH_SCALE`**:
  Phoneme length scale for the Piper application. Controls the speed of speech.
  A value greater than `1.0` slows down the speech, while a value less than
  `1.0` speeds it up.

  **Default**: `1.0`

- **`PIPER_NOISE_SCALE`**:
  Scale for generator noise in the Piper application. Controls the
  expressiveness of the speech. Higher values make the speech more expressive
  but potentially less stable.

  **Default**: `1.0`

- **`PIPER_NOISE_W`**:
  Width noise for phonemes in the Piper application. Controls the variation in
  phoneme duration. Higher values introduce more variation, making the speech
  sound more dynamic.

  **Default**: `1.0`

- **`PIPER_SENTENCE_SILENCE`**:
  Duration of silence (in seconds) after each sentence in the Piper
  application. Useful for adding natural pauses between sentences.

  **Default**: `0.5`

### HTTP and Caching Configuration

These variables control HTTP request behavior and caching settings.

- **`HTTP_AGENT`**:
  The user agent string used for HTTP requests, such as when downloading voice
  models. This string identifies the application and its version to remote
  servers.

  **Default**: 'oremi-izwi/<version>'

### API and Logging Configuration

These variables control the API root path, logging behavior, and server
settings.

- **`ROOT_PATH`**:
  Defines the base URL path for all API routes.

  **Default**: `'/'`

- **`LOG_FILE`**:
  Path to the log file for storing application logs. If empty, logs are printed
  to the console.

  **Default**: `''` (logs to console)

- **`LOG_LEVEL`**:
  Controls the verbosity of logs. Options: `'DEBUG'`, `'INFO'`, `'WARNING'`,
  `'ERROR'`, `'CRITICAL'`.

  **Default**: `'INFO'`

### Server Configuration

These variables control the server's host and port settings.

- **`SERVER_HOST`**:
  The host address on which the server will listen.

  **Default**: `'0.0.0.0'`

- **`SERVER_PORT`**:
  The port on which the server will listen.

  **Default**: `5215`

### Performance Configuration

Settings that control the server's performance.

- **`WORKERS`**:
  The number of worker processes for handling requests. By default, this is set
  to the number of CPU cores on the system.

  **Default**: Number of CPU cores
