# Copyright 2023-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from ..core.models import TranscoderModel


French = TranscoderModel(
  lang='fr',
  mdtr={
    'title_level_1': 'Titre',
    'title_level_2': 'Section',
    'title_level_x': 'Sous section',
    'code_block': 'block de code ignoré',
    'image': 'Image :',
  },
  special_chars={
    # unhandled '
    'c\'': 'c',
    'd\'': 'd',
    'j\'': 'j',
    'l\'': 'l',
    'm\'': 'm',
    'n\'': 'n',
    's\'': 's',
    't\'': 't',
    '\'h': '',
    # time
    '00h': 'minuit',
    '01h': 'une heure',
    '02h': 'deux heures',
    '03h': 'trois heures',
    '04h': 'quatre heures',
    '05h': 'cinq heures',
    '06h': 'six heures',
    '07h': 'sept heures',
    '08h': 'huit heures',
    '09h': 'neuf heures',
    '10h': 'dix heures',
    '11h': 'onze heures',
    '12h': 'douze heures',
    '13h': 'treize heures',
    '14h': 'quatorze heures',
    '15h': 'quinze heures',
    '16h': 'seize heures',
    '17h': 'dix-sept heures',
    '18h': 'dix-huit heures',
    '19h': 'dix-neuf heures',
    '20h': 'vingt heures',
    '21h': 'vingt-et-une heures',
    '22h': 'vingt-deux heures',
    '23h': 'vingt-trois heures',
    # weather
    '°C': 'degrés Celsius',
    '°F': 'degrés Fahrenheit',
    # symbols
    '%': 'pour cent',
    '€': 'euros',
    '$': 'dollars',
    '£': 'livres sterling',
    '¥': 'yens',
    '₹': 'roupies',
    '¢': 'centimes',
    '§': 'paragraphe',
    '©': 'copyright',
    '®': 'marque déposée',
    '™': 'marque commerciale',
    '‰': 'pour mille',
    'µ': 'micro',
    '×': 'multiplié par',
    '÷': 'divisé par',
    '≠': 'différent de',
    '≈': 'environ',
    '+': 'plus',
    '*': 'multiplié par',
    '/': 'divisé par',
    '<': 'inférieur à',
    '>': 'supérieur à',
    '=': 'égal à',
    '≤': 'inférieur ou égal à',
    '≥': 'supérieur ou égal à',
    '∑': 'somme de',
    '∆': 'delta',
    '∞': 'infini',
    '√': 'racine carrée de',
    '∫': 'intégrale de',
    '∂': 'partiel',
    '∏': 'produit de',
    '∀': 'pour tout',
    '∈': 'appartient à',
    '∉': 'n\'appartient pas à',
    '∅': 'ensemble vide',
    # units
    'm/s': 'mètres par seconde',
    'km/h': 'kilomètres par heure',
    'mph': 'miles par heure',
    'm²': 'mètres carrés',
    'cm²': 'centimètres carrés',
    'mm²': 'millimètres carrés',
    'm³': 'mètres cubes',
    'cm³': 'centimètres cubes',
    'mm³': 'millimètres cubes',
    'Hz': 'hertz',
    'kHz': 'kilohertz',
    'MHz': 'mégahertz',
    'GHz': 'gigahertz',
    'kPa': 'kilopascals',
    'MPa': 'mégapascals',
    'GPa': 'gigapascals',
  },
)
