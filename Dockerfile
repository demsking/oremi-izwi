#
## Stage 1: Build environment
FROM python:3.10-slim AS install-dependencies

ENV PATH="/root/.local/bin:$PATH"
ENV POETRY_HOME='/usr/local'
ENV POETRY_VIRTUALENVS_CREATE=false

# Set the shell to /bin/bash and enable pipefail
SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

# Install build dependencies
RUN apt-get update \
  && apt-get install -y --no-install-recommends curl \
  && curl -sSL https://install.python-poetry.org | python3 - \
  && rm -rf /var/lib/apt/lists/*

WORKDIR /src
COPY poetry.lock pyproject.toml ./
RUN poetry install --no-root --no-interaction --no-ansi --only=main

#
## Stage 2: Runtime environment
FROM python:3.10-slim AS install-piper

ARG PIPER_VERSION

# Get the latest version of the code
RUN apt-get update \
  && apt-get install -y --no-install-recommends git python3-pip \
  && pip install --no-cache-dir --upgrade pip \
  && git clone --depth 1 --single-branch https://github.com/rhasspy/piper /tmp/piper \
  && cd /tmp/piper \
  && git checkout ${PIPER_VERSION} \
  && cd /tmp/piper/src/python_run \
  && pip install --no-cache-dir -e . \
  && pip install --no-cache-dir -r requirements.txt \
  && mkdir -p /opt/rhasspy/piper \
  && cp -R piper /opt/rhasspy \
  && rm -rf /var/lib/apt/lists/*

# Copy installed Python packages to a temporary location
RUN mkdir -p /opt/python-packages \
  && cp -R /usr/local/lib/python3.10/site-packages/* /opt/python-packages/

#
## Stage 3: Runtime environment
FROM python:3.10-slim

# Copy built Python dependencies from the install-dependencies stage
COPY --from=install-dependencies /usr/local/lib/python3.10/site-packages/ /usr/local/lib/python3.10/site-packages/
COPY --from=install-dependencies /usr/local/bin/ /usr/local/bin/

# Copy Python packages from the install-piper stage
COPY --from=install-piper /opt/python-packages /usr/local/lib/python3.10/site-packages

# Copy built Piper from the install-piper stage
COPY --from=install-piper /opt/rhasspy /opt/rhasspy

# Binaries
COPY bin/* /opt/oremi/bin/
RUN chmod +x /opt/oremi/bin/*

# Copy application files
COPY pyproject.toml LICENSE DOCUMENTATION.md /opt/oremi/
COPY izwi/ /opt/oremi/izwi

ENV PATH="/opt/oremi/bin:/usr/local/bin:$PATH"
ENV PYTHONPATH="/opt/oremi:/opt/rhasspy:/usr/local/lib/python3.10/site-packages"
ENV PYTHONUNBUFFERED="1"
ENV PYTHONDONTWRITEBYTECODE="1"

ENV TZ="Africa/Douala"

ENV LOG_LEVEL="info"
ENV LOG_FILE=

# Enable CPU runtime
ENV RUNTIME="cpu"

# Models directory configuration
ENV MODELS_DIR="/usr/local/cache/piper/models"

# Piper speaker ID configuration
ENV PIPER_SPEAKER_ID="0"

# Phoneme length
ENV PIPER_LENGTH_SCALE="1.0"

# Generator noise
ENV PIPER_NOISE_SCALE="1.0"

# Phoneme width noise
ENV PIPER_NOISE_W="1.0"

# Seconds of silence after each sentence
ENV PIPER_SENTENCE_SILENCE="0.5"

# The host address on which the server will listen
ENV SERVER_HOST="0.0.0.0"

# The port on which the server will listen
ENV SERVER_PORT="5215"

EXPOSE 5215
VOLUME /usr/local/cache/piper/models

ENTRYPOINT ["/opt/oremi/bin/entrypoint.sh"]
