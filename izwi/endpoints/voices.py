# Copyright 2024-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from fastapi import FastAPI
from fastapi import HTTPException
from fastapi import Path

from ..core.controller import Controller
from ..core.models import VoiceInfo


def create_voices_endpoints(tag: str, api: FastAPI, controller: Controller):
  @api.get(
    path='/voices',
    tags=[tag],
    responses={
      200: {
        'content': {
          'application/json': {
            'example': [
              {
                'name': 'ca_ES-upc_ona-medium',
                'language': {
                  'code': 'ca_ES',
                  'family': 'ca',
                  'region': 'ES',
                  'name_native': 'Català',
                  'name_english': 'Catalan',
                  'country_english': 'Spain'
                },
                'quality': 'medium',
                'num_speakers': 1,
                'speaker_id_map': {},
                'aliases': []
              },
              {
                'name': 'ca_ES-upc_ona-x_low',
                'language': {
                  'code': 'ca_ES',
                  'family': 'ca',
                  'region': 'ES',
                  'name_native': 'Català',
                  'name_english': 'Catalan',
                  'country_english': 'Spain'
                },
                'quality': 'x_low',
                'num_speakers': 1,
                'speaker_id_map': {},
                'aliases': [
                  'ca-upc_ona-x-low'
                ]
              }
            ],
          },
        },
      },
    },
  )
  async def get_voices() -> list[VoiceInfo]:
    """
    Retrieve a list of all available voices for text-to-speech synthesis.

    This endpoint provides detailed information about each voice, including its
    name, language, quality, number of speakers, and available speaker IDs.
    This information can be used to select an appropriate voice for synthesis
    tasks.
    """
    return controller.voices_listing

  @api.get(
    path='/voices/{name}',
    tags=[tag],
    responses={
      200: {
        'content': {
          'application/json': {
            'example': {
              'name': 'ca_ES-upc_ona-medium',
              'language': {
                'code': 'ca_ES',
                'family': 'ca',
                'region': 'ES',
                'name_native': 'Català',
                'name_english': 'Catalan',
                'country_english': 'Spain'
              },
              'quality': 'medium',
              'num_speakers': 1,
              'speaker_id_map': {},
              'aliases': []
            },
          },
        },
      },
      404: {'description': 'Voice not found'},
    },
  )
  async def get_voice_info(
    name: str = Path(
      description='The name of the voice to retrieve information about.',
    ),
  ) -> VoiceInfo:
    """
    Retrieve detailed information about a specific voice by its name.

    This endpoint provides metadata about a specific voice, including its
    language, quality, number of speakers, and available speaker IDs.
    It is useful for obtaining detailed information about a single voice
    for synthesis tasks.
    """
    if name in controller.voices_map:
      return controller.voices_map[name]

    raise HTTPException(status_code=404, detail='Voice not found')

  @api.head(
    path='/voices/{name}',
    tags=[tag],
    responses={
      404: {'description': 'Voice not found'},
    },
  )
  async def preload_voice(
    name: str = Path(
      description='The name of the voice to preload for faster synthesis.',
    ),
  ):
    """
    Preload a voice model into memory for faster synthesis in subsequent
    requests.

    This endpoint is used to optimize performance by loading a voice model
    into memory before it is needed for synthesis.
    The `HEAD` method is used to indicate that no response body is returned,
    only the status code.
    """
    try:
      controller.load_voice(name)
    except ValueError as error:
      raise HTTPException(status_code=404, detail='Voice not found') from error
