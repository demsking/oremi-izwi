# Original source: https://github.com/rhasspy/piper/blob/d09b7d02567958162ff86a4a42476740bc3b8a02/src/python_run/piper/download.py
import hashlib
from pathlib import Path


def get_file_hash(path: str | Path, bytes_per_chunk: int = 8192) -> str:
  """Hash a file in chunks using md5."""
  path_hash = hashlib.md5()
  with open(path, 'rb') as path_file:
    chunk = path_file.read(bytes_per_chunk)
    while chunk:
      path_hash.update(chunk)
      chunk = path_file.read(bytes_per_chunk)

  return path_hash.hexdigest()
