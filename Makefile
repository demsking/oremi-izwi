APP_NAME := $(shell python metadata.py name)
APP_VERSION := $(shell python metadata.py version)
IMAGE_NAME := demsking/$(APP_NAME)

# Define the common build arguments and labels
IMAGE_COMMON_ARGS := \
  --build-arg PIPER_VERSION="$(PIPER_VERSION)" \
  --label "org.opencontainers.image.title=$(APP_NAME)" \
  --label "org.opencontainers.image.description=$(shell python metadata.py description)" \
  --label "org.opencontainers.image.version=$(APP_VERSION)" \
  --label "org.opencontainers.image.revision=$(shell git rev-parse HEAD)" \
  --label "org.opencontainers.image.authors=$(shell python metadata.py authors)" \
  --label "org.opencontainers.image.created=$(shell date --rfc-3339=seconds)" \
  --label "org.opencontainers.image.source=$(shell python metadata.py repository)" \
  --label "org.opencontainers.image.url=$(shell python metadata.py repository)" \
  --label "org.opencontainers.image.documentation=$(shell python metadata.py documentation)" \
  --label "org.opencontainers.image.vendor=Oremi" \
  --label "org.opencontainers.image.licenses=$(shell python metadata.py license)"

# Start the development environment using tmuxinator
shell:
	tmuxinator

stop:
	tmux kill-session -t oremi-izwi

install:
	poetry install

up:
	touch .env.local
	docker compose up --build -d --remove-orphans

restart:
	docker compose restart

watch:
	docker compose watch cpu

down:
	docker compose down

config:
	docker compose config

logs:
	docker compose logs -f

ps:
	docker compose ps

stats:
	docker compose stats

gen-tests:
	dotenv -- python gen.py
	pre-commit run double-quote-string-fixer

test: gen-tests
	pytest -vv

test-watch: gen-tests
	nodemon -e py -x 'dotenv -- pytest -vv'

coverage: gen-tests
	pytest --cov=izwi

coverage-html: gen-tests
	pytest --cov=izwi --cov-report=html

open-docs:
	open http://localhost:15215/docs

lint:
	pre-commit run --all-files

fix:
	ruff check . --fix

# Show outdated dependencies using poetry
outdated:
	poetry show --outdated

update:
	poetry -C python update
	devbox update
	pre-commit autoupdate

clean:
	rm -rf dist/

test-cmd:
	time curl --silent -X POST -H 'Content-Type: application/json' \
	  --data '@tests/data/long.json' 'localhost:15215/synthesize/stream' \
	  | ffplay -autoexit -nodisp -i -

	time curl --silent -X POST -H 'Content-Type: application/json' \
	  --data '@tests/data/long.json' 'localhost:15215/synthesize' \
	  | ffplay -autoexit -nodisp -i -

	time curl --silent -X POST -H 'Content-Type: application/json' \
	  --data '{"voice":"fr_FR-siwis-medium","text":"Bonjour tout le monde."}' 'localhost:15215/synthesize/stream' \
	  | ffplay -autoexit -nodisp -i -

	time curl --silent -X POST -H 'Content-Type: application/json' \
	  --data '{"voice":"fr_FR-siwis-medium","text":"Bonjour tout le monde."}' 'localhost:15215/synthesize' \
	  | ffplay -autoexit -nodisp -i -

#
## Packaging
build/context-cpu:
	docker buildx create --name $(APP_NAME)-cpu --bootstrap
	mkdir -p build/
	touch $@

build/context-gpu:
	docker buildx create --name $(APP_NAME)-gpu --bootstrap
	mkdir -p build/
	touch $@

image-cpu: build/context-cpu
	docker buildx use $(APP_NAME)-cpu
	docker buildx build . \
	  --platform linux/amd64,linux/arm64 \
	  --push \
	  --progress plain \
	  --tag $(IMAGE_NAME):$(APP_VERSION) \
	  --tag $(IMAGE_NAME):latest \
	  $(IMAGE_COMMON_ARGS)

image-gpu: build/context-gpu
	docker buildx use $(APP_NAME)-gpu

	# GPU version is only supported on linux/amd64
	docker buildx build . \
	  --platform linux/amd64 \
	  --push \
	  --progress plain \
	  --file GPU.Dockerfile \
	  --tag $(IMAGE_NAME):$(APP_VERSION)-gpu \
	  --tag $(IMAGE_NAME):latest-gpu \
	  $(IMAGE_COMMON_ARGS)

image:
	$(MAKE) -j2 image-cpu image-gpu

image-test:
	# docker build --debug . \
	docker build . \
	  --progress plain \
	  --tag $(IMAGE_NAME):latest \
	  $(IMAGE_COMMON_ARGS)

	# docker build --debug . \
	docker build . \
	  --progress plain \
	  --file GPU.Dockerfile \
	  --tag $(IMAGE_NAME):latest-gpu \
	  $(IMAGE_COMMON_ARGS)

# Function to Send Notifications
notify = notify-send -t 3000 -a "$(IMAGE_NAME)" -h "int:transient:1" $(1) $(2)

_watch-image-test:
	$(call notify, "Building image", "Building image $(IMAGE_NAME):$(APP_VERSION)...")
	$(MAKE) image-test
	$(call notify, "Building done", "Image $(IMAGE_NAME):$(APP_VERSION) built successfully")
	$(call notify, "Publishing image", "Publishing image $(IMAGE_NAME):$(APP_VERSION)...")
	docker push $(IMAGE_NAME):latest
	$(call notify, "Publishing done", "Publishing image $(IMAGE_NAME):$(APP_VERSION) done")

watch-image-test:
	nodemon -e py -x $(MAKE) _watch-image-test

testserver: image-test
	docker run -it --rm -v ~/.cache/piper/models:/usr/local/cache/piper/models -p 5215:5215 -e WORKER=1 $(IMAGE_NAME)

publish: image-test
	git commit pyproject.toml -m "Release $(APP_VERSION)"
	$(MAKE) image
	git tag v$(APP_VERSION)
	git push --tags origin main
