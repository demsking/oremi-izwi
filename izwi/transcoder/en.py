# Copyright 2024-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
from ..core.models import TranscoderModel


English = TranscoderModel(
  lang='en',
  mdtr={
    'title_level_1': 'Title',
    'title_level_2': 'Section',
    'title_level_x': 'Subsection',
    'code_block': 'code block omitted',
    'image': 'Image:',
  },
  special_chars={
    # weather
    '°C': 'degrees Celsius',
    '°F': 'degrees Fahrenheit',
    # symbols
    '%': 'percent',
    '€': 'euros',
    '$': 'dollars',
    '£': 'pounds sterling',
    '¥': 'yen',
    '₹': 'rupees',
    '¢': 'cents',
    '§': 'section',
    '©': 'copyright',
    '®': 'registered trademark',
    '™': 'trademark',
    '‰': 'per mille',
    'µ': 'micro',
    '×': 'multiplied by',
    '÷': 'divided by',
    '≠': 'not equal to',
    '≈': 'approximately',
    '+': 'plus',
    '*': 'multiplied by',
    '/': 'divided by',
    '<': 'less than',
    '>': 'greater than',
    '=': 'equal to',
    '≤': 'less than or equal to',
    '≥': 'greater than or equal to',
    '∑': 'sum of',
    '∆': 'delta',
    '∞': 'infinity',
    '√': 'square root of',
    '∫': 'integral of',
    '∂': 'partial',
    '∏': 'product of',
    '∀': 'for all',
    '∈': 'belongs to',
    '∉': 'does not belong to',
    '∅': 'empty set',
    'm/s': 'meters per second',
    'km/h': 'kilometers per hour',
    'mph': 'miles per hour',
    'm²': 'square meters',
    'cm²': 'square centimeters',
    'mm²': 'square millimeters',
    'm³': 'cubic meters',
    'cm³': 'cubic centimeters',
    'mm³': 'cubic millimeters',
    'Hz': 'hertz',
    'kHz': 'kilohertz',
    'MHz': 'megahertz',
    'GHz': 'gigahertz',
    'kPa': 'kilopascals',
    'MPa': 'megapascals',
    'GPa': 'gigapascals',
  },
)
