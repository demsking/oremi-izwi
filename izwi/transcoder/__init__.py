# Copyright 2023-2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
import re

from ..core.handle_text import prepare_tts_input_with_context
from ..core.models import TranscoderModel


class Transcoder:
  def __init__(
    self,
    models: list[TranscoderModel],
  ):
    self.models = {
      model.lang: model for model in models
    }

    self.variants = {
      model.lang: sorted(model.special_chars.items(), key=lambda x: len(x[0]), reverse=True)
      for model in models
    }

  def parse(self, lang: str, *, text: str):
    model = self.models[lang]
    text = prepare_tts_input_with_context(text, model.mdtr)

    if lang in self.variants:
      special_chars = self.variants[lang]

      for special_char, word in special_chars:
        if special_char in text:
          text = text.replace(special_char, f' {word} ' if len(word) > 1 else word)

      return re.sub(r'\s+', ' ', text)

    return text
