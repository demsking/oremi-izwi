# Copyright 2025 Sébastien Demanou. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
import re

import emoji

from .models import TranscoderMarkdownTranslation

# Note:
# This function is adapted from the original source:
# https://github.com/travisvn/openai-edge-tts/commit/552a9eb772eb8be1cc014df5ee29f2d410c997e3#diff-3c3f05d2da99796455ef71ba8269d2507c7c463ea426bc70816fb4b7107a9e92


def prepare_tts_input_with_context(text: str, tr: TranscoderMarkdownTranslation) -> str:
  """
  Prepares text for a TTS API by cleaning Markdown and adding minimal contextual hints
  for certain Markdown elements like headers. Preserves paragraph separation.
  """

  # Remove emojis
  text = emoji.demojize(text)
  text = re.sub(r':[a-zA-Z_]+:', '', text)  # Remove demojized text

  # Add context for headers
  def header_replacer(match):
    level = len(match.group(1))  # Number of '#' symbols
    header_text = match.group(2).strip()

    if level == 1:
      return f"{tr['title_level_1']} — {header_text}\n"

    if level == 2:
      return f"{tr['title_level_2']} — {header_text}\n"

    return f"{tr['title_level_x']} — {header_text}\n"

  text = re.sub(r'^(#{1,6})\s+(.*)', header_replacer, text, flags=re.MULTILINE)

  # Announce links (currently commented out for potential future use)
  # text = re.sub(r"\[([^\]]+)\]\((https?:\/\/[^\)]+)\)", r"\1 (link: \2)", text)

  # Remove links while keeping the link text
  text = re.sub(r'\[([^\]]+)\]\([^\)]+\)', r'\1', text)

  # Describe inline code
  text = re.sub(r'`([^`]+)`', r'code snippet: \1', text)

  # Remove bold/italic symbols but keep the content
  text = re.sub(r'(\*\*|__|\*|_)', '', text)

  # Remove code blocks (multi-line) with a description
  text = re.sub(r'```([\s\S]+?)```', rf'({tr["code_block"]})', text)

  # Remove image syntax but add alt text if available
  text = re.sub(r'!\[([^\]]*)\]\([^\)]+\)', rf'{tr["image"]} \1', text)

  # Remove HTML tags
  text = re.sub(r'</?[^>]+(>|$)', '', text)

  # Normalize line breaks
  text = re.sub(r'\n{2,}', '\n\n', text)  # Ensure consistent paragraph separation

  # Replace multiple spaces within lines
  text = re.sub(r' {2,}', ' ', text)

  # Trim leading and trailing whitespace from the whole text
  text = text.strip()

  return text
